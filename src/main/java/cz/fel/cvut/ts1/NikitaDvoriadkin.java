package cz.fel.cvut.ts1;

public class NikitaDvoriadkin {

    public long factorial(int n) {

        if (n < 0)
            throw new UnsupportedOperationException();

        long fact = 1;
        for (int a = n; a > 0; a--)
            fact *= a;

        return fact;
    }

    public long factorialRecursive(int n) {
        if (n == 0)
            return 1;
        else return n * factorialRecursive(n-1);
    }
}
