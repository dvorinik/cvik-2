package cz.fel.cvut.ts1;

import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class NikitaDvoriadkinTest {

    @org.junit.jupiter.api.Test
    void factorial() {

        final NikitaDvoriadkin nd = new NikitaDvoriadkin();
        long one = nd.factorial(0);
        long two = nd.factorial(1);
        long three = nd.factorial(5);
        long four = nd.factorial(10);

        assertEquals(one, 1);
        assertEquals(two, 1);
        assertEquals(three, 120);
        assertEquals(four, 3628800);
        assertThrows(UnsupportedOperationException.class,
                () -> nd.factorial(-5));

    }
}